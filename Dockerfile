FROM node:16.11.1

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install

RUN chmod +x ./node_modules/.bin/vite

COPY . .

EXPOSE 3000

CMD ["yarn", "dev"]
