import {ActionIcon} from "@mantine/core";
import {IconHeart} from "@tabler/icons-react";
import {FC} from "react";

type AddRecipeToFavoritesButtonPropsType = {
    onClick: () => void
    isFavorite: boolean
}

export const AddRecipeToFavoritesButton: FC<AddRecipeToFavoritesButtonPropsType> = ({onClick, isFavorite}) => {
    return (
        <ActionIcon
            onClick={onClick}
            variant={isFavorite ? 'filled' : 'transparent'}
            color='red'
        >
            <IconHeart/>
        </ActionIcon>
    )
}