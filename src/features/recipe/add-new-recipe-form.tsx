import {useForm} from "@mantine/form";
import {Button, Flex, TextInput} from "@mantine/core";
import {RecipeType} from "../../App.tsx";

export const AddNewRecipeForm = ({onSubmit, onCancel}: {
    onSubmit: (value: RecipeType) => void,
    onCancel: () => void
}) => {
    const form = useForm({
        initialValues: {
            src: '',
            title: '',
            description: ''
        }
    })

    const handleSubmit = (data: typeof form.values) => {
        onSubmit({
            title: data.title,
            description: data.description,
            isFavorite: false,
            createdByUser: true,
            image: {
                src: data.src,
                description: data.title
            }
        })
        form.reset()
    }

    const handleCancel = () => {
        onCancel()
    }

    return (
        <form onSubmit={form.onSubmit(handleSubmit)}>
            <Flex direction='column' gap={12}>
                <TextInput
                    label='Изображение рецепта'
                    placeholder='Введите ссылку на изображение рецепта'
                    {...form.getInputProps('src')}
                />
                <TextInput
                    label='Название'
                    placeholder='Введите название'
                    {...form.getInputProps('title')}
                />
                <TextInput
                    label='Описание рецепта'
                    placeholder='Введите описание рецепта'
                    {...form.getInputProps('description')}
                />
                <Flex gap={12} justify='space-between'>
                    <Button type='submit'>
                        Добавить
                    </Button>
                    <Button variant='outline' type='button' onClick={handleCancel}>
                        Отмена
                    </Button>
                </Flex>
            </Flex>
        </form>
    )
}