import {ActionIcon, Card, Flex, Text} from "@mantine/core";
import {IconPlus} from "@tabler/icons-react";
import {useState} from "react";
import {AddNewRecipeForm} from "./add-new-recipe-form.tsx";
import {RecipeType} from "../../App.tsx";

export const AddNewRecipe = ({addNewRecipe}: { addNewRecipe: (value: RecipeType) => void }) => {
    const [isEdit, toggleIsEdit] = useState(false)

    return (
        <Card w='100%' miw={300} maw={360} shadow="sm" padding="lg" radius="md" withBorder>
            {isEdit
                ? <AddNewRecipeForm
                    onSubmit={addNewRecipe}
                    onCancel={() => toggleIsEdit(prevState => !prevState)}
                />
                : <Flex align='center' justify='center' gap={4}>
                    <Text fw={500}>Добавление нового рецепта</Text>
                    <ActionIcon size='sm' onClick={() => toggleIsEdit(prevState => !prevState)}><IconPlus/></ActionIcon>
                </Flex>
            }
        </Card>
    )
}