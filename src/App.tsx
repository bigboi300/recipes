import "@mantine/core/styles.css";
import {Box, Flex, MantineProvider, ScrollArea, Tabs, TextInput} from "@mantine/core";
import {theme} from "./theme.ts";
import {Header} from "./widgets/header/ui/header.tsx";
import {Footer} from "./widgets/footer/ui/footer.tsx";
import {RecipeCardList} from "./widgets/recipe/ui/recipe-card-list.tsx";
import {useMemo, useState} from "react";
import {IconSearch} from "@tabler/icons-react";

export type RecipeType = {
    title: string,
    description: string,
    isFavorite: boolean,
    createdByUser: boolean
    image: {
        src: string | null,
        description: string
    }
}

const data: RecipeType[] = [
    {
        title: "СТРИПЛОЙН С ЧИМИЧУРРИ",
        description: 'Стриплойн, обжаренный на гриле, с обжаренным чесноком и тимьяном на сливочном масле. Отдельно подается соус Чимичурри.',
        isFavorite: false,
        createdByUser: false,
        image: {
            src: 'https://thumb.tildacdn.com/tild3162-6330-4133-b633-623539326263/-/cover/560x420/center/center/-/format/webp/cd506ab1234b234645dc.jpeg',
            description: 'СТРИПЛОЙН С ЧИМИЧУРРИ'
        }
    },
    {
        title: "ТАЛЬОЛИНИ С КАМЧАТСКИМ КРАБОМ И ТРЮФЕЛЬНЫМ МАСЛОМ",
        description: 'Тальолини с крабом, томатами конкассе и луком шалот приготовленные на раковом биске , поливается трюфельным маслом,и посыпается женным фундуком и кресс салатом.',
        isFavorite: true,
        createdByUser: false,
        image: {
            src: 'https://static.tildacdn.com/tild6561-6666-4438-b038-616337643933/6ba0bf4c0fe33816952c.jpeg',
            description: 'ТАЛЬОЛИНИ С КАМЧАТСКИМ КРАБОМ И ТРЮФЕЛЬНЫМ МАСЛОМ'
        }
    },
    {
        title: "ВЕГАН БУРГЕР С РАСТИТЕЛЬНОЙ КОТЛЕТОЙ",
        description: 'Черная булочка обжаренная на гриле, растительная котлета (тофу, гречка, растительный белок-соевый, рисовый), маринованные огурцы, томаты, лук красный, соус Барбекю.',
        isFavorite: false,
        createdByUser: false,
        image: {
            src: 'https://static.tildacdn.com/tild3634-6165-4164-b064-383563396434/IMG_9862_1_1.jpg',
            description: 'ВЕГАН БУРГЕР С РАСТИТЕЛЬНОЙ КОТЛЕТОЙ'
        }
    },
    {
        title: "ПИЦЦА С ФОРЕЛЬЮ И ДРЕВЕСНЫМИ ГРИБАМИ",
        description: 'Римская пицца с соусом том ям, моцарелла, кусочки лосося, соус Унаги , древесные грибы. Украшается слайсами редиса, кинзой, кунжутом и зеленым маслом.',
        isFavorite: false,
        createdByUser: false,
        image: {
            src: 'https://static.tildacdn.com/tild3039-3832-4333-a134-346431333734/1a996ecc8065d4122914.jpeg',
            description: "ПИЦЦА С ФОРЕЛЬЮ И ДРЕВЕСНЫМИ ГРИБАМИ",
        }
    },
]

export default function App() {
    const [recipes, setRecipes] = useState<RecipeType[]>(data)

    const [searchQuery, setSearchQuery] = useState<string>('')
    const [searchDescrQuery, setSearchDescrQuery] = useState<string>('')

    const filteredRecipesByTitle = useMemo(() => {
        if (searchQuery) return recipes.filter(recipe => recipe.title.toLowerCase().includes(searchQuery.toLowerCase()))
        return recipes
    }, [recipes, searchQuery])

    const filteredRecipesByDescr = useMemo(() => {
        if (searchDescrQuery) return filteredRecipesByTitle.filter(recipe => recipe.description.toLowerCase().includes(searchDescrQuery.toLowerCase()))
        return filteredRecipesByTitle
    }, [filteredRecipesByTitle, searchDescrQuery])

    const addToFavourite = (title: string) => {
        setRecipes(
            prev =>
                prev.map(
                    recipe =>
                        recipe.title === title
                            ? {
                                ...recipe,
                                isFavorite: !recipe.isFavorite
                            }
                            : recipe
                )
        )
    }

    const addNewRecipe = (recipe: RecipeType) => {
        setRecipes(
            prev =>
                [...prev, recipe]
        )
    }

    return (
        <MantineProvider theme={theme}>
            <ScrollArea h="100vh">
                <Box maw={1280} m='auto' px={32}>
                    <Flex direction='column' gap={32} justify='end'>
                        <Header/>
                        <Tabs defaultValue="list">
                            <Tabs.List>
                                <Tabs.Tab value="list">
                                    Рецепты
                                </Tabs.Tab>
                                <Tabs.Tab value="favorites">
                                    Избранное
                                </Tabs.Tab>
                                <Tabs.Tab value="settings">
                                    Добавленные
                                </Tabs.Tab>
                            </Tabs.List>

                            <Flex mt={16} mb={16} gap={16}>
                                <TextInput
                                    label="Поиск по названию блюда"
                                    placeholder='Введите название блюда'
                                    rightSection={
                                        <IconSearch/>
                                    }
                                    value={searchQuery}
                                    onChange={(e) => setSearchQuery(e.currentTarget.value)}
                                />
                                <TextInput
                                    label="Поиск по описанию блюда"
                                    placeholder='Введите описание блюда'
                                    rightSection={
                                        <IconSearch/>
                                    }
                                    value={searchDescrQuery}
                                    onChange={(e) => setSearchDescrQuery(e.currentTarget.value)}
                                />
                            </Flex>

                            <Tabs.Panel value="list">
                                <RecipeCardList
                                    data={filteredRecipesByDescr}
                                    addToFavourite={(value) => addToFavourite(value)}
                                    addNewRecipe={(value: RecipeType) => addNewRecipe(value)}
                                />
                            </Tabs.Panel>

                            <Tabs.Panel value="favorites">
                                <RecipeCardList
                                    data={filteredRecipesByDescr.filter(recipe => recipe.isFavorite)}
                                    addToFavourite={(value) => addToFavourite(value)}
                                    addNewRecipe={(value: RecipeType) => addNewRecipe(value)}
                                />
                            </Tabs.Panel>

                            <Tabs.Panel value="settings">
                                <RecipeCardList
                                    data={filteredRecipesByDescr.filter(recipe => recipe.createdByUser)}
                                    addToFavourite={(value) => addToFavourite(value)}
                                    addNewRecipe={(value: RecipeType) => addNewRecipe(value)}
                                />
                            </Tabs.Panel>
                        </Tabs>
                        <Footer/>
                    </Flex>
                </Box>
            </ScrollArea>
        </MantineProvider>
    );
}
