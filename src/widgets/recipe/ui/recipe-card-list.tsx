import {RecipeCard} from "../../../entities/recipe/ui/recipe-card.tsx";
import {AddRecipeToFavoritesButton} from "../../../features/recipe/add-recipe-to-favorites.tsx";
import {Flex} from "@mantine/core";
import {RecipeType} from "../../../App.tsx";
import {AddNewRecipe} from "../../../features/recipe/add-new-recipe.tsx";

export const RecipeCardList = ({data, addToFavourite, addNewRecipe}: {
    data: RecipeType[],
    addToFavourite: (value: string) => void
    addNewRecipe: (value: RecipeType) => void
}) => {

    const handleClick = (title: string) => {
        addToFavourite(title)
    }

    return (
        <Flex gap={24} justify={'space-between'} wrap='wrap'>
            {data.map(recipe => (
                <RecipeCard
                    key={recipe.title}
                    title={recipe.title}
                    description={recipe.description}
                    cardAction={
                        <AddRecipeToFavoritesButton
                            onClick={() => handleClick(recipe.title)}
                            isFavorite={recipe.isFavorite}
                        />
                    }
                    image={recipe.image}
                />
            ))}
            <AddNewRecipe addNewRecipe={addNewRecipe}/>
        </Flex>
    )
}