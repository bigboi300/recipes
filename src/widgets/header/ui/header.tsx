import {Center} from "@mantine/core";

export const Header = () => {
    return (
        <header>
            <Center py={16}>
                Книга рецептов
            </Center>
        </header>
    )
}