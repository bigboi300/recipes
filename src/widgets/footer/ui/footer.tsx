import {useMemo} from "react";

export const Footer = () => {
    const date = useMemo(() => {
        return new Date().getFullYear()
    }, [])

    return (
        <footer>
            ©, {date}
        </footer>
    )
}