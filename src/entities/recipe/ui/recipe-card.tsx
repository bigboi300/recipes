import {FC, ReactNode} from "react";
import {Card, Group, Image, Text} from "@mantine/core";

type RecipeCardPropsType = {
    title: string
    description: string
    cardAction?: ReactNode
    image: {
        src: string | null
        description: string
    }
}

export const RecipeCard: FC<RecipeCardPropsType> = ({title, description, cardAction, image}) => {

    return (
        <Card miw={300} maw={360} shadow="sm" padding="lg" radius="md" withBorder>
            <Card.Section>
                <Image
                    height={200}
                    src={image.src}
                    fallbackSrc={'https://thumb.tildacdn.com/tild3162-6330-4133-b633-623539326263/-/cover/560x420/center/center/-/format/webp/cd506ab1234b234645dc.jpeg'}
                    alt={image.description}
                />
            </Card.Section>

            <Group w="100%" h={64} justify="space-between" mt="md" mb="xs" wrap='nowrap'>
                <Text fw={500}>{title}</Text>
                {cardAction}
            </Group>

            <Text size="sm" c="dimmed">
                {description}
            </Text>
        </Card>
    )
}